#!/bin/bash

TAVERSION=5.37.18
TADATE="14-Apr-2023"

###########################################################################
if [ -z "$REQUEST_METHOD" -o "$1" = "version" ]; then

  echo TopolA TAdmin User App Menu
  echo Version: ${TAVERSION}
  echo Compiled at: ${TADATE}
  echo "Compiled by: Oleg Vlasenko (vop@unity.net)"
  echo Site: http://topola.unity.net/
  if [ "$1" = "version" ] && [ !  -z "$2" ]; then
    # echo Access: All
    echo AdmAccess: taduapp Papers
    echo Util: apivers 2.0
    echo Menu: Papers
    echo MenuLink: Plugin demonstates access to clients papers
    # echo Util: plugconf
    echo Util: subdir adm
    # echo Util: mainmenu
    echo Util: uappmenu show=it
  fi
  echo "Copyright (c) 2015, Oleg Vlasenko"

  if [ "$1" != "version" ]; then
    echo
    echo ERROR! This scrip must be using as CGI-script or as plugin
  fi
  
  exit 0
fi
###########################################################################
##### Parse query string
saveIFS=$IFS
IFS='=&'
parm=($QUERY_STRING)
IFS=$saveIFS

for ((i=0; i<${#parm[@]}; i+=2))
do
    declare qstr_${parm[i]}=${parm[i+1]}
done
###########################################################################
if [ "${qstr_cus}" ]; then

  DOCS_DIR="${DOCUMENT_ROOT}/adm/docs/${qstr_cus}"

  if [ -d "${DOCS_DIR}" ]; then

    # Go to WebRoot page (User page)
    if [ "$HTTPS" = "on" ]; then
      echo "Location: https://${HTTP_HOST}/adm/docs/${qstr_cus}"
    else
      echo "Location: http://${HTTP_HOST}/adm/docs/${qstr_cus}"
    fi
    echo
    exit 0
  fi
fi

# You can use any regular CGI scription from this point instead Location.

echo Content-Type: text/plain
echo

echo This client has not a document directory at "${DOCS_DIR}"

# declare
exit 0
###########################################################################
